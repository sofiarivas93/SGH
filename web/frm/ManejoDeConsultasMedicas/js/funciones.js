/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function agregarCita() {

    var datosFormulario = $("#formPrograma").serialize();
    $.ajax({
        type: 'POST',
        url: '../jsp/crearCita.jsp',
        data: datosFormulario,
        dataType: 'json',
        beforeSend: function (objeto) {
            $("#mensajes").html("Enviando datos al Servidor ...");
        },
        success: function (json) {
            $("#mensajes").html(json.mensaje);
            //limpiarFormulario();
            //$("#usuario_paciente").focus();
            //$("#usuario_paciente").select();
        },
        error: function (e) {
            $("#mensajes").html("No se pudo agregar los datos.");
        },
        complete: function (objeto, exito, error) {
            $("#usuario_paciente").focus();
        }
    });
}


function agregarPaciente() {

    var datosFormulario = $("#formPrograma").serialize();
    $.ajax({
        type: 'POST',
        url: '../jsp/agregarPaciente.jsp',
        data: datosFormulario,
        dataType: 'json',
        beforeSend: function (objeto) {
            $("#mensajes").html("Enviando datos al Servidor ...");
        },
        success: function (json) {
            $("#mensajes").html(json.mensaje);
            limpiarFormulario();
            $("#usuario_paciente").focus();
            $("#usuario_paciente").select();
        },
        error: function (e) {
            $("#mensajes").html("No se pudo agregar los datos.");
        },
        complete: function (objeto, exito, error) {
            $("#usuario_paciente").focus();
        }
    });
}
function agregarMedico() {
    
    var datosFormulario = $("#formPrograma").serialize();
    $.ajax({
        type: 'POST',
        url: '../jsp/agregarMedico.jsp',
        data: datosFormulario,
        dataType: 'json',
        beforeSend: function (objeto) {
            $("#mensajes").html("Enviando datos al Servidor ...");

        },
        success: function (json) {
            $("#mensajes").html(json.mensaje);
            //limpiarFormulario();

            $("#usuario_medico").focus();
            $("#usuario_medico").select();
        },
        error: function (e) {
            $("#mensajes").html("No se pudo agregar los datos.");
        },
        complete: function (objeto, exito, error) {
            $("#usuario_medico").focus();
        }
    });
}
function limpiarFormulario() {
    $("#usuario_paciente").val("");
    $("#password_paciente").val("");
    $("#nombre_paciente").val("");
    $("#apellido_paciente").val("");
    $("#ci_paciente").val("0");
    $("#edad_paciente").val("0");
}
function validarFormulario() {
    var valor = true;
    if ($("#nombre_paciente").val().trim() === "") {
        valor = false;
        $("#mensajes").html("Nombre no puede estar vacio.");
        $("#nombre_paciente").focus();
    }

    return valor;
}

function buscarPacientes() {

    $.ajax({
        type: "POST",
        url: "../jsp/pacienteBusqueda.jsp",
        dataType: 'json',
        
        success: function (json)
        {
            $("#lista_pacientes").html(json.pacientes).fadeIn();
           
        }
    });

}
function buscarEspecialidades() {

    $.ajax({
        type: "POST",
        url: "../jsp/especialidadBusqueda.jsp",
        dataType: 'json',
        
        success: function (json)
        {
            $("#lista_especialidades").html(json.especialidades).fadeIn();
           
        }
    });

}

function buscarMedicos() {
 var datosFormulario = $("#formPrograma").serialize();

    $.ajax({
        data: datosFormulario,
        type: "POST",
        url: "../jsp/medicoBusqueda.jsp",
        dataType: 'json',
        success: function (json)
        {
            $("#selector_medicos").prop('hidden', false);
            $("#lista_medicos").html(json.medicos).fadeIn();
            
            
        }
    });

}
function buscarHorarios() {
 var datosFormulario = $("#formPrograma").serialize();

    $.ajax({
        data: datosFormulario,
        type: "POST",
        url: "../jsp/horarioBusqueda.jsp",
        dataType: 'json',
        success: function (json)
        {
            $("#selector_horarios").prop('hidden', false);
            $("#lista_horarios").html(json.horarios).fadeIn();
            
            
        }
    });

}
function buscarDiadeSemana(){
    var fech=document.getElementById("fecha1").value;
    var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"); 
    var f=new Date(fech);
    $("#fecha").val(diasSemana[f.getDay()+1]);     
        
}
function buscarDia() {

    $.ajax({
        type: "POST",
        url: "../jsp/buscarDia.jsp",
        dataType: 'json',
        success: function (json)
        {
            $("#lista_dia").html(json.dia).fadeIn();

        }
    });
}
function buscarHora() {

    $.ajax({
        type: "POST",
        url: "../jsp/buscarHora.jsp",
        dataType: 'json',
        success: function (json)
        {
            $("#mostrar_hora").prop('hidden', false);
            $("#lista_hora").html(json.hora).fadeIn();
            
        }
    });

}
function buscarNombrePaciente() {
    var datosFormulario = $("#formBuscar").serialize();
    $.ajax({
        type: 'POST',
        url: '../jsp/buscarPaciente.jsp',
        data: datosFormulario,
        dataType: 'json',
        beforeSend: function (objeto) {
            $("#mensajes").html("Enviando datos al Servidor ...");
            $("#contenidoBusqueda").css("display", "none");
        },
        success: function (json) {
            $("#mensajes").html(json.mensaje);
            $("#contenidoBusqueda").html(json.contenido);
            $("#contenidoBusqueda").fadeIn("slow");
            $("tbody tr").on("click", function () {
                var id = $(this).find("td:first").html();
                $("#panelBuscar").html("");
                $("#id_paciente").val(id);
                $("#nombre_paciente").focus();
                buscarIdPaciente();
                $("#buscar").fedeOut("slow");
                $("#panelPrograma").fadeIn("slow");
            });
        },

        error: function (e) {
            $("#mensajes").html("No se pudo buscar registros.");
        },
        complete: function (objeto, exito, error) {
            if (exito === "success") {

            }
        }
    });
}


function buscarIdPaciente() {
    var datosFormulario = $("#formPrograma").serialize();
    //alert(datosFormulario);
    $.ajax({
        type: 'POST',
        url: '../jsp/buscarIdPaciente.jsp',
        data: datosFormulario,
        dataType: 'json',
        beforeSend: function (objeto) {
            $("#mensajes").html("Enviando datos al Servidor ...");
        },
        success: function (json) {
            $("#mensajes").html(json.mensaje);
            $("#id_paciente").val(json.id_rubro);
            $("#nombre_paciente").val(json.nombre_rubro);
            
           
        },

        error: function (e) {
            $("#mensajes").html("No se pudo recuperar los datos.");
        },
        complete: function (objeto, exito, error) {
            if (exito === "success") {

            }
        }
    });
}