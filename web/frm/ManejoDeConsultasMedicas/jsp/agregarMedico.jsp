<%-- 
    Document   : agregar
    Created on : 18-may-2018, 9:26:08
    Author     : ALUMNO
--%>
<%@page import="modelos.Especialidades"%>
<%@page import="modelos.Horarios"%>
<%@page import="controladores.MedicosControlador"%>
<%@page import="modelos.Medicos"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="java.sql.ResultSet"%>
<%       
    //int id_medico = Integer.parseInt(request.getParameter("id_medico"));
    //System.out.println("entro aqui en jsp");
    String usuario_medico = request.getParameter("usuario_medico");
    String password_medico = request.getParameter("password_medico");
    String nombre_medico = request.getParameter("nombre_medico");
    String apellido_medico = request.getParameter("apellido_medico");
    int ci_medico = Integer.parseInt( request.getParameter("ci_medico"));
    int edad_medico = Integer.parseInt(request.getParameter("edad_medico"));
    String[] sexo_medico = request.getParameterValues("sexo_medico");
    String[] especialidades = request.getParameterValues("lista_especialidades");
    String[] dia = request.getParameterValues("lista_dia");
    int registro =Integer.parseInt(request.getParameter("registro"));

    
    String tipo = "error";
    String mensaje = "Datos no agregados.";
    Especialidades especial=new Especialidades();
    especial.setId_especialidad(Integer.parseInt(especialidades[0]));
    
    Horarios horario=new Horarios();
    horario.setId_horario(Integer.parseInt(dia[0])); 
    
    Medicos medico = new Medicos();
   
    medico.setUsuario_medico(usuario_medico);
    medico.setPassword_medico(password_medico);
    medico.setNombre_medico(nombre_medico);
    medico.setCi_medico(ci_medico);
    medico.setApellido_medico(apellido_medico);
    medico.setEdad_medico(edad_medico);
    medico.setRegistro(registro);
    medico.setSexo_medico(sexo_medico[0]);
    medico.setHorario(horario);
    medico.setEspecialidad(especial);   
 
    if (MedicosControlador.agregar(medico)) {
        tipo = "success";
        mensaje = "Datos agregados.";
    }
    
    JSONObject obj = new JSONObject();
    obj.put("tipo", tipo);
    obj.put("mensaje", mensaje);
    out.print(obj);
    out.flush();
%>


