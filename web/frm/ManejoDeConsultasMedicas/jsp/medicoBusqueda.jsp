<%-- 
    Document   : buscarId
    Created on : 11-abr-2018, 9:32:37
    Author     : Ester
--%>


<%@page import="modelos.Medicos"%>
<%@page import="controladores.MedicosControlador"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="java.sql.ResultSet"%>
<%
   
   int especialidad =Integer.parseInt(request.getParameter("lista_especialidades"));
    String tipo = "error";
    String mensaje = "Datos no encontrados.";
    String nuevo = "true";
    String medicos = MedicosControlador.buscarMedicos(especialidad);
   
    if (medicos != null) {
        tipo = "success";
        mensaje = "Datos encontrados.";
        nuevo = "false";

    } else {
        mensaje="No se tienen medicos para esa especialidad";
        
    }
    JSONObject obj = new JSONObject();

    obj.put("tipo", tipo);
    obj.put("mensaje", mensaje);
    //obj.put("nuevo", nuevo);
    obj.put("medicos", medicos);
  
    out.print(obj);
    out.flush();
%>