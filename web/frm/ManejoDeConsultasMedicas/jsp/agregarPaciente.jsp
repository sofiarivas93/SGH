<%-- 
    Document   : agregar
    Created on : 18-may-2018, 9:26:08
    Author     : ALUMNO
--%>
<%@page import="controladores.PacientesControlador"%>
<%@page import="modelos.Pacientes"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="java.sql.ResultSet"%>
<%       
    
    //int id_paciente = Integer.parseInt(request.getParameter("id_paciente"));
    String usuario_paciente = request.getParameter("usuario_paciente");
    String password_paciente = request.getParameter("password_paciente");
    String nombre_paciente = request.getParameter("nombre_paciente");
    String apellido_paciente = request.getParameter("apellido_paciente");
    int ci_paciente = Integer.parseInt( request.getParameter("ci_paciente"));
    int edad_paciente = Integer.parseInt(request.getParameter("edad_paciente"));
    String[] sexo_paciente = request.getParameterValues("sexo_paciente");
    
    String tipo = "error";
    String mensaje = "Datos no agregados.";

    Pacientes paciente = new Pacientes();
    paciente.setUsuario_paciente(usuario_paciente);
    paciente.setPassword_paciente(password_paciente);
    paciente.setNombre_paciente(nombre_paciente);
    paciente.setApellido_paciente(apellido_paciente);
    paciente.setCi_paciente(ci_paciente);
    paciente.setEdad_paciente(edad_paciente);
    paciente.setSexo_paciente(sexo_paciente[0]);
    
    
    if (PacientesControlador.agregar(paciente)) {
        tipo = "success";
        mensaje = "Datos agregados.";
    }
    
    JSONObject obj = new JSONObject();
    obj.put("tipo", tipo);
    obj.put("mensaje", mensaje);
    out.print(obj);
    out.flush();
%>


