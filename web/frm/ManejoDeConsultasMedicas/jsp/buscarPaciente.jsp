<%-- 
    Document   : buscarNombre
    Created on : 11-abr-2018, 9:32:54
    Author     : Ester
--%>


<%@page import="controladores.PacientesControlador"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="java.sql.ResultSet"%>
<%
    String nombre_paciente = request.getParameter("bnombre_paciente");
   // int pagina = Integer.parseInt(request.getParameter("bpagina"));

    String mensaje = "Busqueda exitosa.";
    String contenido = PacientesControlador.buscarNombrePaciente(nombre_paciente);

    JSONObject obj = new JSONObject();
    obj.put("mensaje",mensaje);
    obj.put("contenido", contenido);

    out.print(obj);
    out.flush();
%>