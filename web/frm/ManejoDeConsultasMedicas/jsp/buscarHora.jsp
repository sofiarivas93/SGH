
<%@page import="controladores.HorariosControlador"%>
<%@page import="java.util.LinkedList"%>
<%@page import="modelos.Horarios"%>
<%@page import="modelos.Medicos"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="java.sql.ResultSet"%>
<%
    String dia = request.getParameter("lista_dia");
    String tipo = "error";
    String mensaje = "Datos no encontrados.";
    String nuevo = "true";
    
    String hora = HorariosControlador.buscarHora(dia); 
    System.out.println(hora);
    if (hora != null) {
        tipo = "success";
        mensaje = "Datos encontrados.";
        nuevo = "false";   
    }
    JSONObject obj = new JSONObject();
    
    obj.put("tipo", tipo);

    obj.put("hora", hora);
    
    out.print(obj);
    out.flush();
%>