<%-- 
    Document   : buscarId
    Created on : 11-abr-2018, 9:32:37
    Author     : Ester
--%>


<%@page import="controladores.PacientesControlador"%>
<%@page import="modelos.Pacientes"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="java.sql.ResultSet"%>
<%
    int id_paciente = Integer.parseInt(request.getParameter("id_paciente"));
    String tipo = "error";
    String mensaje = "Datos no encontrados.";
    String nuevo = "true";
    //Rubros paciente = new Rubros();
    //paciente.setId_paciente(id_paciente);
    Pacientes paciente = PacientesControlador.buscarIdPaciente(id_paciente);
    if (paciente != null) {
        tipo = "success";
        mensaje = "Datos encontrados.";
        nuevo = "false";

    } else {
        paciente = new Pacientes();
        paciente.setId_paciente(id_paciente);
        paciente.setNombre_paciente("");
    }
    JSONObject obj = new JSONObject();

    obj.put("tipo", tipo);
    obj.put("mensaje", mensaje);
    obj.put("nuevo", nuevo);
    obj.put("id_paciente", paciente.getId_paciente());
    obj.put("nombre_paciente", paciente.getNombre_paciente());
    out.print(obj);
    out.flush();
%>