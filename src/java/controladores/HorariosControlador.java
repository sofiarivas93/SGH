/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import modelos.Horarios;
import utiles.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utiles.Utiles;

/**
 *
 * @author ALUMNO
 */
public class HorariosControlador {

    public static String buscarDia() {

        String dia = "";

        if (Conexion.conectar()) {
            try {
                /*String sql = "select * from especialidades where nombre_especialidad like '%" +
                nombre+ "%'";*/
                String sql = "select * from horarios";

                //System.out.println("—-—>" + sql);
                try (PreparedStatement ps = Conexion.getConn().prepareStatement(sql)) {
                    ResultSet rs = ps.executeQuery();
                    dia+="<option value='0'> Seleccionar</option>";
                    while (rs.next()) {

                        dia += "<option value='" + Integer.parseInt(rs.getString("id_horario")) + "'>" + rs.getString("dia") + "</option>";
                    }
                    ps.close();

                } catch (SQLException ex) {
                    System.err.println("Error:" + ex);
                }
                Conexion.cerrar();
            } catch (Exception ex) {
                System.err.println("Errort " + ex);
            }
        }
        Conexion.cerrar();

        return dia;
    }

    public static String buscarHora(String dia) {

        String hora = "";

        if (Conexion.conectar()) {
            try {

                String sql = "select * from horarios where horarios.dia = ' " + dia + "'";

                try (PreparedStatement ps = Conexion.getConn().prepareStatement(sql)) {
                    ResultSet rs = ps.executeQuery();
                    hora+="<option value='0'> Seleccionar</option>";
                    while (rs.next()) {

                        hora += "<option value='" + Integer.parseInt(rs.getString("id_horario")) + "'>" + rs.getString("hora") + "</option>";
                    }
                    ps.close();

                } catch (SQLException ex) {
                    System.err.println("Error:" + ex);
                }
                Conexion.cerrar();
            } catch (Exception ex) {
                System.err.println("Errort " + ex);
            }
        }
        System.out.println(hora);
        Conexion.cerrar();

        return hora;
    }

    public static String buscarHorarios(int id, String dia) {
        String horarios = "";

        if (Conexion.conectar()) {
            try {
                
                String sql = "select * from horarios h where h.id_medico='" + id + "' and h.dia='" + dia + "'";

                System.out.println("—-—>" + sql);
                try (PreparedStatement ps = Conexion.getConn().prepareStatement(sql)) {
                    ResultSet rs = ps.executeQuery();
                    horarios+="<option value='0'> Seleccionar</option>";
                    while (rs.next()) {

                        horarios += "<option value='" + Integer.parseInt(rs.getString("id_horario")) + "'>" + rs.getString("hora") + "</option>";
                        /*especialidades += "<tr>"
                        + "<td>" + rs.getString("id_especialidad") + "</td>"
                        + "<td>" + rs.getString("nombre_especialidad") + "</td>"
                        + "</tr>";*/
                    }
                    /* if (especialidades.equals("")) {
                       especialidades = "<tr><td colspan=2>No existen registros...</td></tr>";
                    }*/
                    //System.out.println(especialidades);

                    ps.close();

                } catch (SQLException ex) {
                    System.err.println("Error:" + ex);
                }
                Conexion.cerrar();
            } catch (Exception ex) {
                System.err.println("Error " + ex);
            }
        }
        Conexion.cerrar();

        return horarios;
    }
}

