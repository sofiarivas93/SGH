/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;
import modelos.Pacientes;
import utiles.Conexion;
import utiles.Utiles;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 *
 * @author ALUMNO
 */
public class PacientesControlador {
    public static boolean agregar(Pacientes paciente){
        boolean valor=false;
        if (Conexion.conectar()){
            String sql="insert into pacientes(usuario_paciente,nombre_paciente, apellido_paciente, ci_paciente, edad_paciente, "
                    + "sexo_paciente, password_paciente)" + "values('"+paciente.getUsuario_paciente()+"','"
                    +paciente.getNombre_paciente()+"','"+paciente.getApellido_paciente()+
                    "','"+paciente.getCi_paciente()+"','"+paciente.getEdad_paciente()+"','"+paciente.getSexo_paciente()+"','"
                    +Utiles.md5(Utiles.quitarGuiones(paciente.getPassword_paciente())) +"')";
            System.out.println(sql);
            try{
                Conexion.getSt().executeUpdate(sql);
                valor=true;
            }catch (SQLException ex){
                System.err.println("Error: "+ex);
            }
        }
        return valor;
    }
    public static Pacientes buscarIdPaciente(int paciente_id) {
       
        Pacientes paciente = new Pacientes();
        if (Conexion.conectar()) {
            String sql = "select * from pacientes where id_paciente='" + paciente_id + "'";
            System.out.println(sql);
            try{
                ResultSet rs = Conexion.getSt().executeQuery(sql);

                if (rs.next()) {
                    
                    paciente.setId_paciente(rs.getInt("id_paciente"));
                    paciente.setNombre_paciente(rs.getString("nombre_paciente"));
                    //return paciente;
                } else {
                    //paciente.setId_paciente(0);
                    //paciente.setNombre_paciente("");
                    return null;
                    //return paciente;
                }
            } catch (SQLException ex) {
                System.out.println("Error: " + ex);
            }
        }     
        
        return paciente;
    }
    public static String buscarNombrePaciente(String nombre) {
        //int offset = (pagina - 1) * Utiles.REGISTROS_PAGINA;
        String valor = "";
        if (Conexion.conectar()) {
            try {
                String sql = "select * from pacientes p where p.nombre_paciente like '%" +
                nombre+ "%'";
                //+ " order by id_paciente offset " + offset + " limit " + Utiles.REGISTROS_PAGINA;
                System.out.println("—-—>" + sql);
                try (PreparedStatement ps = Conexion.getConn().prepareStatement(sql)) {
                    ResultSet rs = ps.executeQuery();
                    String tabla = "";
                    while (rs.next()) {
                        tabla += "<tr>"
                        + "<td>" + rs.getString("id_paciente") + "</td>"
                        + "<td>" + rs.getString("nombre_paciente") + "</td>"
                                + "<td>" + rs.getString("apellido_paciente") + "</td>"
                        + "</tr>";
                    }
                    if (tabla.equals("")) {
                        tabla = "<tr><td colspan=2>No existen registros...</td></tr>";
                    }
                    ps.close();
                    valor = tabla;
                } catch (SQLException ex) {
                    System.err.println("Error:" + ex);
                }
                Conexion.cerrar();
            } catch (Exception ex) {
                System.err.println("Errort " + ex);
            }
        }
        Conexion.cerrar();
        return valor;
    }
public static String buscarPacientes(){
        String pacientes = "";
        
        if (Conexion.conectar()) {
            try {
                /*String sql = "select * from pacientes where nombre_paciente like '%" +
                nombre+ "%'";*/
                String sql ="select * from pacientes";
                
                //System.out.println("—-—>" + sql);
                try (PreparedStatement ps = Conexion.getConn().prepareStatement(sql)) {
                    ResultSet rs = ps.executeQuery();
                    pacientes+="<option value='0'> Seleccionar</option>";
                    
                    while (rs.next()) {
                        
                        pacientes+="<option value='"+Integer.parseInt(rs.getString("id_paciente"))+"'>"+rs.getString("nombre_paciente")+" "+rs.getString("apellido_paciente")+"</option>";
                        /*pacientes += "<tr>"
                        + "<td>" + rs.getString("id_paciente") + "</td>"
                        + "<td>" + rs.getString("nombre_paciente") + "</td>"
                        + "</tr>";*/
                    }
                   /* if (pacientes.equals("")) {
                       pacientes = "<tr><td colspan=2>No existen registros...</td></tr>";
                    }*/
                    //System.out.println(pacientes);
                    
                    ps.close();
                    
                } catch (SQLException ex) {
                    System.err.println("Error:" + ex);
                }
                Conexion.cerrar();
            } catch (Exception ex) {
                System.err.println("Errort " + ex);
            }
        }
        Conexion.cerrar();
        
        return pacientes;
    }
}
