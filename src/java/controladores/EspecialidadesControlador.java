/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import modelos.Especialidades;
import utiles.Conexion;
import utiles.Utiles;

/**
 *
 * @author ALUMNO
 */
public class EspecialidadesControlador {
    public static String buscarEspecialidades(){
        String especialidades = "";
        
        if (Conexion.conectar()) {
            try {
                /*String sql = "select * from especialidades where nombre_especialidad like '%" +
                nombre+ "%'";*/
                String sql ="select * from especialidades";
                
                //System.out.println("—-—>" + sql);
                try (PreparedStatement ps = Conexion.getConn().prepareStatement(sql)) {
                    ResultSet rs = ps.executeQuery();
                    especialidades+="<option value='0'> Seleccionar</option>";
                    
                    while (rs.next()) {
                        
                        especialidades+="<option value='"+Integer.parseInt(rs.getString("id_especialidad"))+"'>"+rs.getString("nombre_especialidad")+"</option>";
                        /*especialidades += "<tr>"
                        + "<td>" + rs.getString("id_especialidad") + "</td>"
                        + "<td>" + rs.getString("nombre_especialidad") + "</td>"
                        + "</tr>";*/
                    }
                   /* if (especialidades.equals("")) {
                       especialidades = "<tr><td colspan=2>No existen registros...</td></tr>";
                    }*/
                    //System.out.println(especialidades);
                    
                    ps.close();
                    
                } catch (SQLException ex) {
                    System.err.println("Error:" + ex);
                }
                Conexion.cerrar();
            } catch (Exception ex) {
                System.err.println("Errort " + ex);
            }
        }
        Conexion.cerrar();
        
        return especialidades;
    }
public static Especialidades buscarId(int especialidad_id) {
       
        Especialidades especialidad = new Especialidades();
        System.out.println("entro");
        if (Conexion.conectar()) {
            String sql = "select * from especialidades where id_especialidad='" + especialidad_id + "'";
            System.out.println(sql);
            try{
                ResultSet rs = Conexion.getSt().executeQuery(sql);

                if (rs.next()) {
                    
                    especialidad.setId_especialidad(rs.getInt("id_especialidad"));
                    especialidad.setNombre_especialidad(rs.getString("nombre_especialidad"));
                    //return especialidad;
                } else {
                    //especialidad.setId_especialidad(0);
                    //especialidad.setNombre_especialidad("");
                    return null;
                    //return especialidad;
                }
            } catch (SQLException ex) {
                System.out.println("Error: " + ex);
            }
        }     
        
        return especialidad;
    }
    
}
