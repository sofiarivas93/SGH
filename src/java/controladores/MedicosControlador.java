/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import modelos.Medicos;
import utiles.Conexion;
import utiles.Utiles;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ALUMNO
 */
public class MedicosControlador {

    public static boolean agregar(Medicos medico) {
        boolean valor = false;
        if (Conexion.conectar()) {
            String sql = "insert into medicos(usuario_medico,password_medico,"
                    + " nombre_medico,ci_medico, apellido_medico, edad_medico,registro, "
                    + "sexo_medico, id_horario,id_especialidad )" + "values('"
                    + medico.getUsuario_medico() + "','" + Utiles.md5(Utiles.quitarGuiones(medico.getPassword_medico())) + "','"
                    + medico.getNombre_medico() + "','" + medico.getCi_medico() + "','" + medico.getApellido_medico() + "','"
                    + medico.getEdad_medico() + "','" + medico.getRegistro() + "','" + medico.getSexo_medico() + "','" + medico.getHorario().getId_horario() + "','"
                    + medico.getEspecialidad().getId_especialidad()
                    + "')";

            System.out.println("Entro" + sql);
            try {
                Conexion.getSt().executeUpdate(sql);
                valor = true;
            } catch (SQLException ex) {
                System.err.println("Error: " + ex);
            }
        }
        return valor;
    }
public static String buscarMedicos(int id){
        String medicos = "";
        
        if (Conexion.conectar()) {
            try {
                /*String sql = "select * from especialidades where nombre_especialidad like '%" +
                nombre+ "%'";*/
                String sql ="select * from medicos where medicos.id_especialidad='"+id+"'";
                
                System.out.println("—-—>" + sql);
                try (PreparedStatement ps = Conexion.getConn().prepareStatement(sql)) {
                    ResultSet rs = ps.executeQuery();
                        
                    medicos+="<option value='0'>Seleccionar</option>";
                    while (rs.next()) {
                        
                        medicos+="<option value='"+Integer.parseInt(rs.getString("id_medico"))+"'>"+rs.getString("nombre_medico")+" "+rs.getString("apellido_medico")+"</option>";
                        /*especialidades += "<tr>"
                        + "<td>" + rs.getString("id_especialidad") + "</td>"
                        + "<td>" + rs.getString("nombre_especialidad") + "</td>"
                        + "</tr>";*/
                    }
                   /* if (especialidades.equals("")) {
                       especialidades = "<tr><td colspan=2>No existen registros...</td></tr>";
                    }*/
                    //System.out.println(especialidades);
                    
                    ps.close();
                    
                } catch (SQLException ex) {
                    System.err.println("Error:" + ex);
                }
                Conexion.cerrar();
            } catch (Exception ex) {
                System.err.println("Errort " + ex);
            }
        }
        Conexion.cerrar();
        
        return medicos;
    }
}
