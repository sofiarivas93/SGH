/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;


/**
 *
 * @author ALUMNO
 */
public class Horarios {
    int id_horario;
    private int dia;
    private int hora;
    private int id_registro;
    private int cantidad_pacientes;
    private String sala;//no debe coincidir

    public Horarios() {
    }

    public Horarios(int id_horario, int dia, int hora, int id_registro, int cantidad_pacientes, String sala) {
        this.id_horario = id_horario;
        this.dia = dia;
        this.hora = hora;
        this.id_registro = id_registro;
        this.cantidad_pacientes = cantidad_pacientes;
        this.sala = sala;
    }

    public int getId_horario() {
        return id_horario;
    }

    public void setId_horario(int id_horario) {
        this.id_horario = id_horario;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getId_registro() {
        return id_registro;
    }

    public void setId_registro(int id_registro) {
        this.id_registro = id_registro;
    }

    public int getCantidad_pacientes() {
        return cantidad_pacientes;
    }

    public void setCantidad_pacientes(int cantidad_pacientes) {
        this.cantidad_pacientes = cantidad_pacientes;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    

   
}
