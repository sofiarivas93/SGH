/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author ALUMNO
 */
public class Medicos {
    private int id_medico;
    private String usuario_medico;
    private String password_medico;
    private String nombre_medico;
    private String apellido_medico;
    private int ci_medico;
    private int edad_medico;
    private String sexo_medico;
    private Especialidades especialidad;
    private int registro;
    private Horarios horario;

    public Medicos() {
    }
    
    
 
    public Medicos(int id_medico, String usuario_medico, String password_medico, String nombre_medico, String apellido_medico, int ci_medico, int edad_medico, String sexo_medico, Especialidades especialidad, int registro, Horarios horario) {
        this.id_medico = id_medico;
        this.usuario_medico = usuario_medico;
        this.password_medico = password_medico;
        this.nombre_medico = nombre_medico;
        this.apellido_medico = apellido_medico;
        this.ci_medico = ci_medico;
        this.edad_medico = edad_medico;
        this.sexo_medico = sexo_medico;
        this.especialidad = especialidad;
        this.registro = registro;
        this.horario = horario;
    }

    public int getId_medico() {
        return id_medico;
    }

    public void setId_medico(int id_medico) {
        this.id_medico = id_medico;
    }

    public String getUsuario_medico() {
        return usuario_medico;
    }

    public void setUsuario_medico(String usuario_medico) {
        this.usuario_medico = usuario_medico;
    }

    public String getPassword_medico() {
        return password_medico;
    }

    public void setPassword_medico(String password_medico) {
        this.password_medico = password_medico;
    }

    public String getNombre_medico() {
        return nombre_medico;
    }

    public void setNombre_medico(String nombre_medico) {
        this.nombre_medico = nombre_medico;
    }

    public String getApellido_medico() {
        return apellido_medico;
    }

    public void setApellido_medico(String apellido_medico) {
        this.apellido_medico = apellido_medico;
    }

    public int getCi_medico() {
        return ci_medico;
    }

    public void setCi_medico(int ci_medico) {
        this.ci_medico = ci_medico;
    }

    public int getEdad_medico() {
        return edad_medico;
    }

    public void setEdad_medico(int edad_medico) {
        this.edad_medico = edad_medico;
    }

    public String getSexo_medico() {
        return sexo_medico;
    }

    public void setSexo_medico(String sexo_medico) {
        this.sexo_medico = sexo_medico;
    }

    public Especialidades getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(Especialidades especialidad) {
        this.especialidad = especialidad;
    }

    public int getRegistro() {
        return registro;
    }

    public void setRegistro(int registro) {
        this.registro = registro;
    }

    public Horarios getHorario() {
        return horario;
    }

    public void setHorario(Horarios horario) {
        this.horario = horario;
    }
    
    

}
