/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.sql.Date;

/**
 *
 * @author ALUMNO
 */
public class Consultorio {
    private int id_consulta;
    private Medicos medico;
    private Horarios horario;
    private Date fecha;

    public Consultorio(int id_consulta, Medicos medico, Horarios horario, Date fecha, Citas[] cita) {
        this.id_consulta = id_consulta;
        this.medico = medico;
        this.horario = horario;
        this.fecha = fecha;
        this.cita = cita;
    }
    

    public int getId_consulta() {
        return id_consulta;
    }

    public void setId_consulta(int id_consulta) {
        this.id_consulta = id_consulta;
    }

    public Medicos getMedico() {
        return medico;
    }

    public void setMedico(Medicos medico) {
        this.medico = medico;
    }

    public Horarios getHorario() {
        return horario;
    }

    public void setHorario(Horarios horario) {
        this.horario = horario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Citas[] getCita() {
        return cita;
    }

    public void setCita(Citas[] cita) {
        this.cita = cita;
    }
    private Citas[] cita;
  
}
